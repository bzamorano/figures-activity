from abc import ABC, abstractmethod
from cgi import log


class Shape(ABC):

    @abstractmethod
    def calculateArea(self):
        pass

    @abstractmethod
    def calculatePerimeter(self):
        pass
