from Shape import Shape


class Triangle(Shape):
    base = 0
    height = 0

    def __init__(self, base, height):
        self.base = base
        self.height = height

    def calculateArea(self):
        area = (self.base * self.height) / 2
        return area

    def calculatePerimeter(self):
        perimeter = 3 * self.base
        return perimeter
