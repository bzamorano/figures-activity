from Shape import Shape


class Circle(Shape):
    r = 0
    PI = 3.142

    def __init__(self, r):
        self.r = r

    def calculateArea(self):
        area = self.PI * (self.r * self.r)
        return area

    def calculatePerimeter(self):
        perimeter = 2 * self.PI * self.r
        return perimeter
