import unittest

from Square import Square


class Test_Square(unittest.TestCase):
    squ = None

    def setUp(self) -> None:
        self.squ = Square(10)

    def test_instance(self):
        self.assertNotEqual(self.squ, None)

    def test_square_area(self):
        self.assertEqual(self.squ.calculateArea(), 100)

    def test_square_per(self):
        self.assertEqual(self.squ.calculatePerimeter(), 40)

    if __name__ == '__main__':
        unittest.main()
