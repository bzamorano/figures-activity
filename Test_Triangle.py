import unittest

from Triangle import Triangle


class Test_Triangle(unittest.TestCase):
    squ = None

    def setUp(self) -> None:
        self.tri = Triangle(6, 8)

    def test_instance(self):
        self.assertNotEqual(self.tri, None)

    def test_triangle_area(self):
        self.assertEqual(self.tri.calculateArea(), 24)

    def test_triangle_per(self):
        self.assertEqual(self.tri.calculatePerimeter(), 18)

    if __name__ == '__main__':
        unittest.main()
