import unittest

from Circle import Circle

class Test_Circle(unittest.TestCase):
    cir = None

    def setUp(self) -> None:
        self.cir = Circle(10)

    def test_instance(self):
        self.assertNotEqual(self.cir, None)

    def test_circle_area(self):
        self.assertEqual(round(self.cir.calculateArea()), 314)

    def test_circle_per(self):
        self.assertEqual(round(self.cir.calculatePerimeter()), 63)

    if __name__ == '__main__':
        unittest.main()