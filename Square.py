from Shape import Shape


class Square(Shape):
    base = 0
    def calculateArea(self):
        area = self.base ** 2
        print(area)
        return area
    def calculatePerimeter(self):
        perimeter = self.base * 4
        print(perimeter)
        return perimeter